<?php

namespace app\utils;

use PDO;
use PDOStatement;

class Connection extends PDO
{
    /**
     * Connection constructor.
     * @param array $options
     */
    public function __construct($options = [])
    {
        $dbDriver = getenv('DB_DRIVER');
        $host = getenv('DB_HOST');
        $dbName = getenv('DB_NAME');

        $dsn = $dbDriver . ':host=' . $host . ';dbname=' . $dbName . ';charset=utf8';
        $username = getenv('DB_USERNAME');
        $password = getenv('DB_PASSWORD');

        $default_options = [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];

        $options = array_replace($default_options, $options);
        parent::__construct($dsn, $username, $password, $options);
    }

    /**
     * @param $sql
     * @param null $args
     * @return bool|false|PDOStatement
     * @author Olawale Lawal <wale@cottacush.com>
     */
    public function executeSQL($sql, $args = NULL)
    {
        if (!$args) {
            return $this->query($sql);
        }

        $stmt = $this->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }
}