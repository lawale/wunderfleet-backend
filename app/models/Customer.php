<?php

namespace app\models;

use app\utils\Connection;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Customer
{
    private $error;

    /** @var Connection */
    private $db;

    public function __construct(Connection $db = null)
    {
        $this->db = $db;
    }

    /**
     * @param array $data
     * @return bool|mixed
     * @author Olawale Lawal <wale@cottacush.com>
     */
    public function save(array $data = [])
    {
        try {
            $this->db->beginTransaction();
            $sql = "INSERT INTO `customers`(first_name,last_name,telephone_number,street_address,house_number,city,zip_code,owner,iban) VALUES (:first_name,:last_name,:telephone_number,:street_address,:house_number,:city,:zip_code,:owner,:iban)";
            $this->db->executeSQL($sql, $data);

            $customerId = $this->db->lastInsertId();
            $paymentDataId = $this->sendPaymentData($customerId, $data['iban'], $data['owner']);
            $this->db->executeSQL(
                "UPDATE `customers` SET payment_data_id = :payment_id WHERE id = :id",
                [
                    'id' => $customerId,
                    'payment_id' => $paymentDataId
                ]
            );
            $this->db->commit();
            return $paymentDataId;
        } catch (RequestException $ex) {
            $this->error = $ex->getMessage();
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
        }

        return false;
    }

    /**
     * @param $id
     * @param $iban
     * @param $owner
     * @return mixed
     * @throws Exception
     * @author Olawale Lawal <wale@cottacush.com>
     */
    private function sendPaymentData($id, $iban, $owner)
    {
        $client = new Client();
        $response = $client->request(
            'POST',
            getenv('PAYMENT_URL'),
            [
                'json' => [
                    'customerId' => $id,
                    'iban' => $iban,
                    'owner' => $owner
                ]
            ]
        );

        $body = json_decode($response->getBody());

        if ($response->getStatusCode() != 200) {
            $message = $body->error;
            throw new Exception($message, $response);
        }

        return $body->paymentDataId;
    }

    /**
     * @return string
     * @author Olawale Lawal <wale@cottacush.com>
     */
    public function getError()
    {
        return $this->error;
    }
}
