# WunderFleet Backend Assignment
> Customer registration and payment integration for WunderFleet Backend Assignment

## Requirements
The minimum requirement that your Web server supports PHP 7.2

### Composer
- [Composer](https://getcomposer.org/doc/00-intro.md#using-composer)

### PHP Extensions
- pdo
- json

## Setup
- Install Composer
- Install Composer dependencies,
```bash
$ composer install
```
- Setup database, execute the database query in the `db.sql`
- Replace the values in  `.env` in the `app/env` directory as appropriate.
- Run the project using the PHP inbuilt server
```sh
php -S localhost:8080 -t public/
```


## Questions
### Describe possible performance optimizations for your Code.
- Better use of the Dependency Injection on the project
- Remove the need for jQuery library, since it increase the load time of the page

### Which things could be done better, than you’ve done it?
- Implement the project better in full MVC pattern instead of combining the view and controller
- Use an ORM / ActiveRecord for the model
- Implement server-side validation of form input
- Application of container, e.g Docker, to ease the project setup
- Write both tests, both unit and functional tests
