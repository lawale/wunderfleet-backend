(function ($) {
    'use strict';

    let $steps = $(".step"),
        stepsCount = $steps.length,

        $prevBtn = $("#prev"),
        $nextBtn = $("#next"),
        $submitBtn = $('#submit'),

        navigationKey = 'step-index',
        index = localStorage.getItem(navigationKey) || 0,

        formStorageKey = 'wunder-db',
        formId = '#form',
        form = $(formId),

        canMoveForward = function () {
            return index < stepsCount - 1;
        }, canMoveBackward = function () {
            return index > 0;
        },
        hide = function (element) {
            element.addClass('d-none');
        },
        show = function (element) {
            element.removeClass('d-none');
        },
        saveInputs = function () {
            localStorage.setItem(formStorageKey, JSON.stringify(form.serializeArray()));
        },
        populateInputs = function () {
            if (localStorage.key(formStorageKey)) {
                const savedData = JSON.parse(localStorage.getItem(formStorageKey));
                for (const field of savedData) {
                    console.log(field.name);
                    $(formId + ' [name="' + field.name + '"]').val(field.value);
                }
            }
        },

        move = function (btn, dir, alt) {
            btn.click(function () {
                show(alt);
                hide($submitBtn);

                if (index > 0 && dir === 'previous') {
                    index--;
                } else if (canMoveForward() && dir === 'next') {
                    index++;
                }

                if (index === 0 && dir === 'previous') {
                    hide($(this));
                } else if (index === stepsCount - 1 && dir === 'next') {
                    hide($(this));
                    show($submitBtn);
                }

                saveInputs();
                localStorage.setItem('step-index', index);
                $(".step").removeClass("active").eq(index).addClass("active");
            });
        },

        displayForm = function () {

            $(".step").removeClass("active").eq(index).addClass("active");
            if (canMoveForward()) {
                show($nextBtn);
            }

            if (canMoveBackward()) {
                show($prevBtn);
            }

            if (!canMoveForward()) {
                show($submitBtn);
            }
            populateInputs();
        };

    if (window.isSubmitted === 1) {
        localStorage.removeItem(formStorageKey);
        localStorage.removeItem(navigationKey);
        index = 0;

        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    }

    displayForm();
    move($prevBtn, 'previous', $nextBtn);
    move($nextBtn, 'next', $prevBtn);

    $submitBtn.click(function () {
        saveInputs();
    });
}(jQuery));
