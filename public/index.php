<?php
declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$env = Dotenv\Dotenv::createImmutable(__DIR__ . '/../app/env');
$env->load();
$env->required(['DB_DRIVER', 'DB_HOST', 'DB_NAME', 'DB_USERNAME', 'DB_PASSWORD']);

$isSubmitted = 0;
$error = '';
$paymentDataId = false;

if (!empty($_POST) && $_SERVER['REQUEST_METHOD'] == 'POST') {

    $db = new app\utils\Connection();
    $registration = new app\models\Customer($db);
    $paymentDataId = $registration->save($_POST);

    if (!$paymentDataId) {
        $error = $registration->getError();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Sign Up</title>
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="row">
    <?php if ($paymentDataId) { ?>
        <div class="col-8 text-center py-5 mx-auto">
            <h2 class="pt-2">Congratulations!</h2>
            <h5>You have successfully created your account!</h5>
            <b>Payment ID:</b> <?= $paymentDataId ?>
        </div>
    <?php } else { ?>
        <div class="col-4 mx-auto">
            <form class="pt-5" id="form" method="post" action="">
                <div class="py-5 text-center">
                    <h2>Sign Up</h2>
                    Kindly fill the form below
                </div>
                <?php if ($error) { ?>
                    <div class="my-3">
                        <h6>The following error occurred while submitting the record:</h6>
                        <i><?= $error; ?></i>
                    </div>
                <?php } ?>
                <div class="step">
                    <h5 class="text-muted">Personal Information</h5>
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label for="first_name">First name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name"/>
                        </div>
                        <div class="form-group col-6">
                            <label for="last_name">Last name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telephone_number">Telephone</label>
                        <input type="tel" class="form-control" id="telephone_number" name="telephone_number">
                    </div>
                </div>

                <div class="step">
                    <h5 class="text-muted">Address Information</h5>
                    <div class="form-row">
                        <div class="form-group col-3">
                            <label for="house_number">House #</label>
                            <input type="text" class="form-control" id="house_number" name="house_number">
                        </div>
                        <div class="form-group col-9">
                            <label for="street_address">Street</label>
                            <input type="text" class="form-control" id="street_address" name="street_address">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-6 form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city">
                        </div>
                        <div class="col-6 form-group">
                            <label for="zip_code">Zip Code</label>
                            <input type="text" class="form-control" id="zip_code" name="zip_code">
                        </div>
                    </div>
                </div>

                <div class="step">
                    <h5 class="text-muted">Payment Information</h5>
                    <div class="form-group">
                        <label for="owner">Owner</label>
                        <input type="text" class="form-control" name="owner" id="owner">
                    </div>
                    <div class="form-group">
                        <label for="iban">IBAN</label>
                        <input type="text" class="form-control" id="iban" name="iban">
                    </div>
                </div>

                <button class="btn btn-secondary d-none" type="button" id="prev">PREVIOUS</button>
                <button class="btn btn-primary float-right d-none" type="button" id="next">NEXT</button>
                <button class="btn btn-primary float-right d-none" type="submit" id="submit">NEXT</button>
            </form>
        </div>
    <?php } ?>
</div>
<script> var isSubmitted = <?= $paymentDataId ? '1' : '0'; ?>;</script>
<script src="./assets/js/jquery-3.4.1.slim.min.js"></script>
<script src="./assets/js/app.js"></script>
</body>
</html>
